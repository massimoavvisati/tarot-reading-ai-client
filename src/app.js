import { client } from "@gradio/client";
async function ask(question, card1, card2, card3) {

    const app = await client("https://b1d53bf87d47813aa6.gradio.live");
    const result = await app.predict("/predict", [
        question, // string  in 'text' Textbox component
        card1,
        card2,
        card3
    ]);
    const api_info = await app.view_api(); console.log(api_info);
    console.log(api_info)
    return result.data[0];
}

window.ask = ask;